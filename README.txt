PressPatron
-----------
This module provides limited integration for the PressPatron service.

  https://www.presspatron.com


Features
--------------------------------------------------------------------------------
The primary features include:

* Necessary HTML is inserted at the top of the page.

* A block is available for inserting the banner.

* A block is available for inserting the donate button.


Configuration
--------------------------------------------------------------------------------
 1. On the People Permissions administration page ("Administer >> People
    >> Permissions") assign the "Administer PressPatron" permission to the
    role(s) that are allowed to control the module's settings.

 2. The main settings page controls the account code, which is necessary for the
    banners to work:
      admin/config/services/presspatron

 3. Use the regular block system to position the banner and donate button:
      admin/structure/block


Credits / contact
--------------------------------------------------------------------------------
Developed and maintained by Damien McKenna [1].

The best way to contact the author is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://www.drupal.org/project/issues/presspatron


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/u/damienmckenna
