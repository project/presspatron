/**
 * @file
 * Custom JS for the Presspatron module.
 */

(function (Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.presspatron = {
    attach: function(context) {
      // Make sure the JS isn't loaded twice.
      if (document.getElementById('js-pp-banner')) {
        return;
      }
      var code = drupalSettings.presspatron.code;
      var host = drupalSettings.presspatron.host;
      var fjs = document.getElementsByTagName('script')[0];
      var el = document.createElement('script');
      el.id = 'js-pp-banner';
      el.src = 'https://' + host + '/dev/banner?b=' + code;
      fjs.parentNode.insertBefore(el, fjs);
    }
  }

})(Drupal, drupalSettings);
