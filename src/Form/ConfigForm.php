<?php

namespace Drupal\presspatron\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'presspatron_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'presspatron.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('presspatron.settings')->get();

    $form['code'] = [
      '#title' => t('Account code'),
      '#description' => t('This is taken from the PressPatron account system.'),
      '#type' => 'textfield',
      '#default_value' => $config['code'],
      '#required' => TRUE,
    ];

    $form['host'] = [
      '#title' => t('Host'),
      '#description' => t('Which PressPatron integration environment to use. Use "Production" if unsure.'),
      '#type' => 'select',
      '#options' => [
        'dashboard.presspatron.com' => 'Production',
        'pp-staging.presspatron.com' => 'Staging',
      ],
      '#default_value' => $config['host'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration and save.
    $this->config('presspatron.settings')
      ->set('code', $form_state->getValue('code'))
      ->set('host', $form_state->getValue('host'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
