<?php

namespace Drupal\presspatron\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Banner' Block.
 *
 * @Block(
 *   id = "presspatron_banner",
 *   admin_label = @Translation("PressPatron Banner block"),
 *   category = @Translation("PressPatron"),
 * )
 */
class Banner extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('presspatron.settings')->get();
    return [
      '#theme' => 'presspatron_banner',
      '#attached' => [
        'library' => [
          'presspatron/loader',
        ],
        'drupalSettings' => [
          'presspatron' => [
            'code' => $config['code'],
            'host' => $config['host'],
          ],
        ],
      ],
    ];
  }

}
