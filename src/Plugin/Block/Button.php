<?php

namespace Drupal\presspatron\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Button' Block.
 *
 * @Block(
 *   id = "presspatron_button",
 *   admin_label = @Translation("PressPatron Button block"),
 *   category = @Translation("PressPatron"),
 * )
 */
class Button extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('presspatron.settings')->get();
    return [
      '#theme' => 'presspatron_button',
      '#attached' => [
        'library' => [
          'presspatron/loader',
        ],
        'drupalSettings' => [
          'presspatron' => [
            'code' => $config['code'],
            'host' => $config['host'],
          ],
        ],
      ],
    ];
  }

}
