<?php

namespace Drupal\Tests\presspatron\Functional\Block;

use Drupal\Tests\block\Functional\BlockTestBase;

/**
 * Tests basic block functionality.
 *
 * @group Block
 */
class BlockTest extends BlockTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['block', 'presspatron'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * Test each of the blocks.
   *
   * @dataProvider getBlocksList
   */
  public function testBlock($block_name, $selector) {
    $this->drupalGet('admin/structure/block/add/' . $block_name . '/' . $this->defaultTheme);

    // Save the block so that it will show in the first sidebar on all pages.
    $title = $this->randomMachineName(8);
    $edit = [
      'id' => strtolower($this->randomMachineName(8)),
      'region' => 'sidebar_first',
      'settings[label]' => $title,
      'settings[label_display]' => TRUE,
    ];
    $this->submitForm($edit, t('Save block'));
    $this->assertSession()->pageTextContains('The block configuration has been saved.', 'Block was saved');

    // Verify the block is present on the homepage.
    $this->drupalGet('');
    $this->assertSession()->pageTextContains($title, 'Block was displayed on the front page.');
    $this->assertSession()->elementExists('css', $selector);
  }

  /**
   * A list of all of the blocks provided by this module.
   *
   * @return array
   *   A list of all blocks on the site.
   */
  public function getBlocksList() {
    return [
      ['presspatron_banner', '#pp-banner'],
      ['presspatron_button', '.pp-donate-button'],
    ];
  }

}
