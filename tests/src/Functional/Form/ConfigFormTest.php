<?php

namespace Drupal\Tests\presspatron\Functional\Form;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the Drupal\presspatron\Form\ConfigForm class.
 *
 * @group Form
 */
class ConfigFormTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['presspatron'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Perform initial setup tasks that run before every test method.
   */
  public function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
      'administer presspatron',
    ]);
  }

  /**
   * Tests the \Drupal\presspatron\Form\ConfigForm class.
   */
  public function testConfigForm() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/services/presspatron');
    $this->assertSession()->statusCodeEquals(200);
    $config = $this->config('presspatron.settings');
    $this->assertSession()->fieldValueEquals(
      'code',
      $config->get('code')
    );
    $this->assertSession()->fieldValueEquals(
      'host',
      $config->get('host')
    );
  }

}
